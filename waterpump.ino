// Управление насосом на основании данных объема скважины, и скоростей притока и отбора воды.
// На основании датчика холла вычисляется акт работы насоса и отбора воды.
// Реле отключается как только расчетный объем доходит до 0.
// Реле включится автоматически по 100% наполнению заданного объема, но не ранее чем через 10 сек после выключения.
// для справки по рассчету констант
// время выкачивания в секундах = VOLUME/(PUMP_SPEED-INCOME_SPEED)
// время наполнения в секундах = VOLUME/(INCOME_SPEED)
// а так же лист для подгонки
// https://docs.google.com/spreadsheets/d/1DJlHH3nu58r5djDXL9ysb704EgwEEiSu65epgY6HFSo/edit#gid=0
#define MIN_REST_SEC 10
#define VOLUME 100.0  // Liters
#define INCOME_SPEED 20.0f // liters per min
#define PUMP_SPEED 50.0f // liters per min
#define RELAY_PIN 11
#define SENSOR_PIN A7
#define SENSOR_SENSIVITY_GAIN 600

unsigned long lastMillis = 0L;
unsigned long sensorCount = 0L;
unsigned long sensorSum = 0L;
unsigned long pumpStartMillis = 0L;
unsigned long pumpStopMillis = 0L;
long sensorFlow = 0L;
boolean currentFlow = false;
boolean lastFlow = false;
boolean pumpLastState = false;
float level = VOLUME;

void setup() {
  Serial.begin(9600);
  pinMode(RELAY_PIN, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.println("Ready");
}
void loop() {
  int sensorValue = abs(analogRead(SENSOR_PIN) - 512) * 10;
  sensorSum += sensorValue;
  sensorCount++;
  unsigned long nowMillis = millis();
  unsigned long deltaMillis = nowMillis - lastMillis;
  if (deltaMillis > 1000) {
    sensorFlow = sensorSum / sensorCount;
    currentFlow = sensorFlow > SENSOR_SENSIVITY_GAIN;
    lastMillis = nowMillis;
    level = level + deltaMillis * INCOME_SPEED / 60000.0;
    if (currentFlow) {
      level = level - deltaMillis * PUMP_SPEED / 60000.0;
    }
    if (lastFlow != currentFlow) {
      lastFlow = currentFlow;
      if (currentFlow) {
        pumpStartMillis = nowMillis;
        digitalWrite(LED_BUILTIN, HIGH);
        Serial.print("Pump is on. Last rest for ");
        Serial.print((nowMillis - pumpStopMillis) / 1000);
        Serial.print(" sec.\n");
      } else {
        pumpStopMillis = nowMillis;
        Serial.print("Pump is off. Works for ");
        Serial.print((pumpStopMillis - pumpStartMillis) / 1000);
        Serial.print(" sec.\n");
        digitalWrite(LED_BUILTIN, LOW);
      }
    }
    boolean needrest = (nowMillis - pumpStopMillis < (MIN_REST_SEC * 1000));
    if (level >= VOLUME) {
      level = VOLUME;
      if ((!pumpLastState) && !needrest) {
        Serial.print("Pump is powered. Rest for ");
        Serial.print((nowMillis - pumpStopMillis) / 1000);
        Serial.print(" sec.\n");
        pumpLastState = true;
        digitalWrite(RELAY_PIN, HIGH);
      }
    }
    if ((level <= 0) || needrest) {
      if (level <= 0) {
        level = 0;
      }
      if (pumpLastState) {
        Serial.print("Pump is yielding.\n");
        pumpLastState = false;
        digitalWrite(RELAY_PIN, LOW);
      }
    }
    if (false) {
      Serial.print(" Volume ");
      Serial.print(level);
      Serial.print(", sensor ");
      Serial.print(sensorFlow);
      if (pumpLastState) {
        Serial.print(", powered");
      } else {
        Serial.print(", yielding");
      }
      if (currentFlow) {
        Serial.print(", work = ");
        Serial.print((nowMillis - pumpStartMillis) / 1000);
      }  else {
        Serial.print(", rest = ");
        Serial.print((nowMillis - pumpStopMillis) / 1000);
      }
      Serial.println(" sec");

    }
    sensorCount = 0L;
    sensorSum = 0L;
  }
}
